
OG Path
---------------------------------------------------------

OG Path is a module that slightly alters the path module so that only node aliases belonging to groups of which the user is a member are displayed. This is done so that users can see/edit aliases for their groups while preventing them from complete access to all aliases.

There are no database tables to install; all that is used is a SQL query that selects the appropriate aliases from the {url_alias} table.

Things That Need Work
---------------------------------------------------------
- For some reason, when viewing the aliases for nodes associated with a particular group, the tabbed links at the top won't show. I atruggled this for awhile but had to drop it so that I could focus development on OG Menu.